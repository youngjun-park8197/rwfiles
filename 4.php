<!-- Authority problem, ex) Permission denied
which means, you can get this error message when you run files that are not allowed to read -->

<!-- THe code below, you can check either you cna open that file or not -->

<?php
  $filename = 'readme.txt';
  if (is_readable($filename)) {
      echo 'The file is readable';
  } else {
      echo 'The file is not readable';
  }
?>